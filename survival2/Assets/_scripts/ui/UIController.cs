﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIController : Singleton<UIController> {
	#region 文本显示对象
	public Text totalDay;
	public Text gold;
	public Text diamond;
	public Text bullet;


	public Text debug;
	#endregion
	#region 子对象
	public VJHandler jsMovement;//移动 及方向控制组件
	[Header("UI其它显示对象")]
	#endregion

	#region 玩家控制对象
	private PlayerController playerController;
	#endregion

	/// <summary>
	/// Awake this instance.
	/// 初始化方法
	/// </summary>
	void Awake(){
		base.Awake ();
		playerController = FindObjectOfType<PlayerController> ();
	}
	// Use this for initialization
	void Start () {
		if (totalDay != null) {
			totalDay.text = "第"+GameManager.Instance.liveDays.ToString() + "天";
		}
	}
	
	// Update is called once per frame
	//移动方向控制
	void Update () {
		if (paused) {
			return;
		}
		if (jsMovement.InputDirection.x > 0f) {
//			playerController.Move_Left ();	
			EventService.Instance.GetEvent<ControlPlayerEvent> ().Publish (ControlPlayerEvent.MoveLeft);
		} else if (jsMovement.InputDirection.x < 0f) {
//			playerController.Move_Right ();
			EventService.Instance.GetEvent<ControlPlayerEvent> ().Publish (ControlPlayerEvent.MoveRight);
		} else {
//			playerController.Move_Stop ();
			EventService.Instance.GetEvent<ControlPlayerEvent> ().Publish (ControlPlayerEvent.Stop);
		}
	}
	internal void AddTotalDayTxt(){
		if(totalDay!= null){
			totalDay.text = "第"+GameManager.Instance.liveDays.ToString() +"天";
		}
	} 
	internal void UpdateGunBullet(){
		if (bullet != null) {
//			bullet.text = GameManager.Instance.
		}
	}
	//开枪攻击
	public void Fire(){
		EventService.Instance.GetEvent<ControlPlayerEvent> ().Publish (ControlPlayerEvent.Fire);
	}
	//起跳
	public void Jump(){
		EventService.Instance.GetEvent<ControlPlayerEvent> ().Publish (ControlPlayerEvent.Jump);
	}
	//扔雷
	public void Bobm(){
		EventService.Instance.GetEvent<ControlPlayerEvent> ().Publish (ControlPlayerEvent.Bobm);
	}
	//换枪
	public void ChangeGun(){
		EventService.Instance.GetEvent<ControlPlayerEvent> ().Publish (ControlPlayerEvent.ChangeGun);

	}
	//换子弹
	public void ChangeBullet(){
		EventService.Instance.GetEvent<ControlPlayerEvent> ().Publish (ControlPlayerEvent.ChangeBullet);
	} 


	public void DebugTime(){
		if (Time.timeScale < 1f) {
			Time.timeScale += 0.2f;
		} else {
			Time.timeScale = 0.2f;
		}
		debug.text = "调试速度:" + Time.timeScale; 
	}
}
