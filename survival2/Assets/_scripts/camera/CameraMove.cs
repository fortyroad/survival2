﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour {
	private BasePlayer player;
	public bool isFollowing;
	public float xOffset;
	public float yOffset;
	public float xMin;//低于此值 不移动摄像头
	public float xMax;
	public float yMin;
	public float yMax;


	void Awake(){
		player = FindObjectOfType<BasePlayer> ();
		isFollowing = true;
	}
	// Update is called once per frame
	void Update () {
		if (isFollowing && player != null) {
//			float x = Mathf.Clamp (player.transform.position.x + xOffset, xMin, xMax);
//			float y = Mathf.Clamp (player.transform.position.y + yOffset, yMin, yMax);
			float _x = player.transform.position.x - transform.position.x;
			float _y = player.transform.position.y - transform.position.y;
			transform.position = new Vector3 (transform.position.x + _x, transform.position.y + _y, transform.position.z);

//			transform.position = new Vector3 (x, y, transform.position.z);
		}
	}
		
}
