﻿using UnityEngine;
using System.Collections;

public class PPCamera : MonoBehaviour {
	public	float devHeight = 640f;
    public float devWidth = 960f;
	void Awake(){
		float screenHeight = Screen.height;
		Debug.Log ("screenHeight = " + screenHeight);
		float orthographicSize = GetComponent<Camera> ().orthographicSize;
		float aspectRation = Screen.width * 1.0f / Screen.height;
		float cameraWidth = orthographicSize * 2 * aspectRation;
		Debug.Log ("cameraWidth = " + cameraWidth);
		if (cameraWidth < devWidth / 100) {
			orthographicSize = (devWidth / 100) / (2 * aspectRation);
			GetComponent<Camera> ().orthographicSize = orthographicSize;
			Debug.Log ("new orthographicSize = " + orthographicSize);
		}
	}

}
