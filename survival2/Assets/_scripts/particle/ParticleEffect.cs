﻿using UnityEngine;
using System.Collections;

public class ParticleEffect : MonoBehaviour {
	public static ParticleEffect Instance;
	public ParticleSystem fireEffect;
	void Awake(){
		if (Instance != null) {
			Debug.LogError (" Multiple instances");
		}
		Instance = this;
	}

	/// <summary>
	/// Start this instance.子弹碰撞效果
	/// </summary>
	public void BulletOver(Vector3 pos,string poolName){
		if (null == poolName||poolName.Trim().Equals("")) {
			return;
		}
		GameObject nps = SmartPool.Spawn (poolName);
		if (nps) {
			ParticleSystem	ps = nps.GetComponent<ParticleSystem> ();
			nps.transform.position = pos;
			nps.transform.rotation = Quaternion.identity;
			ps.Play ();
		}
//		GetParticleSystem (fireEffect, pos);
	}
	ParticleSystem GetParticleSystem(ParticleSystem ps,Vector3 pos){
		ParticleSystem newParticleSystem = Instantiate(
			fireEffect,
			pos,
			Quaternion.identity
		) as ParticleSystem;
			

		// Make sure it will be destroyed
		Destroy(
			newParticleSystem.gameObject,
			newParticleSystem.startLifetime
		);

		return newParticleSystem;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


}
