﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleRecyle : MonoBehaviour {
	void OnEnable(){
		StartCoroutine ("RecyleParticle");
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator RecyleParticle(){
		float _seconds = GetComponent<ParticleSystem> ().startLifetime;
		yield return new WaitForSeconds (_seconds);
		SmartPool.Despawn (gameObject);
		StopCoroutine ("RecyleParticle");
	}
}
