﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GearSwitch : MonoBehaviour {

	public RectTransform mainWindow;
	public GameObject[] objs;
	private bool isOpened;
	void Awake(){
		
	}
	// Use this for initialization
	void Start () {
		
			
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private bool pause;
	public void Show(){ 
		/*** 游戏暂停可使此方法
		UnityEngine.Object[] objects = FindObjectsOfType (typeof(GameObject));
		if(!pause){
			pause = true;
			Time.timeScale = 0;
		}else{
			pause = false;
			Time.timeScale = 1;
		}
		foreach (GameObject go in objects) {
			if (pause) {
				go.SendMessage ("OnPauseGame", SendMessageOptions.DontRequireReceiver);
			} else {
				
				go.SendMessage ("OnResumeGame", SendMessageOptions.DontRequireReceiver);
			}
		}
		*/
//		if (gameObject.activeSelf) {
//			return;
//		}
//		gameObject.SetActive (true);
//		mainWindow.localScale = Vector3.zero;
//		LeanTween.scale( mainWindow, new Vector3(1f,1f,1f), 0.6f).setEase(LeanTweenType.easeOutBack);

	}

	public void ChangeSwitchVector(GameObject go){
		
		List<GameObject> _objs = new List<GameObject> ();


		for (int i = 0; i < objs.Length; i++) {
			if (objs [i].name.Equals (go.name)) {
				if (i == 0) {
					_objs.Add (objs [i]);
					_objs.Add (objs [i + 1]);
				} else if (i == objs.Length - 1) {
					_objs.Add (objs [i-1]);
					_objs.Add (objs [i]);
				} else {
					_objs.Add (objs [i-1]);
					_objs.Add (objs [i]);
					_objs.Add (objs [i+1]);
				}
				break;
			}
		}
		for (int i = 0; i < _objs.Count; i++) {
			ChangeObjVector (_objs [i]);
		}
		if (CheckBeOpend()) {
			Debug.Log ("Gear is opened");
			LeanTween.scale (mainWindow, new Vector3 (0f, 0f, 0f), 2f).setEase (LeanTweenType.easeOutSine).setOnComplete(HideGear);
			return;
		}
	}


	private void ChangeObjVector(GameObject go){
		if (go.transform.rotation.z == 0) {
			go.transform.rotation = Quaternion.Euler (0, 0, 180);
		} else {
			go.transform.rotation = Quaternion.Euler (0, 0, 0);
		}
	}
	private bool CheckBeOpend(){
		bool _isUp = true;
		foreach (GameObject go in objs) {
			if (go.transform.rotation.z != 0) {
				_isUp = false;
			}
		}
		isOpened = _isUp;
		return isOpened;
	}
	private void HideGear(){
		Debug.Log ("hide Gear");
		gameObject.SetActive (false);
	}
}
