﻿/// <summary>
/// I enemy.
/// 敌人基本接口
/// </summary>
using UnityEngine;
using System.Collections;
public interface IEnemy{
	//被攻击的接口 hpNum 伤害值 attackType 被攻击的方式
	void BeAttack(float attNum);

	//减速
	void BeSlowVelocity();

	//攻击玩家
	void Attack();

	//死亡
	void Die ();

	//移动
	void Move();

	//重置初始状态
	void Reset();
}