﻿using UnityEngine;
using System.Collections;

public  interface IGun{

	void Fire ();

	void StopFire();
}

