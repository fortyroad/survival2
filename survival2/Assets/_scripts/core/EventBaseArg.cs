﻿/// <summary>
/// 事件基类 带参数事件模型 继承自 EventBase(不带参数)
/// Event base.
/// </summary>

using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
public abstract class EventBaseArg<T> : EventBase
{
	private List<Action<T>> _actions;//保存订冰阅事件处理的方法

	/// <summary>
	/// 场景切换时 ，是否移除该事件对易县的订阅方法
	/// Gets or sets a value indicating whether this <see cref="BaseEvent"/> keep on level changing.
	/// </summary>
	/// <value><c>true</c> if keep on level changing; otherwise, <c>false</c>.</value>
	public bool KeepOnLevelChanging {
		get;
		protected set;
	}

	/// <summary>
	/// 发布事件消息 订阅了相应事件的对象则会接收到响应
	/// Publish this instance.
	/// </summary>
	public void Publish(T payload){
		if (_actions == null) {
			return;
		}
		foreach (var action in _actions) {
			action (payload);
		}
	}
	/// <summary>
	/// 订阅 注册事件 事件列表中添加新事件
	/// Subscribe the specified action.
	/// </summary>
	/// <param name="action">Action.</param>
	public void Subscribe(Action<T> action){
		if (_actions == null) {
			_actions = new List<Action<T>> ();
		}
		if (!_actions.Contains (action)) {
			_actions.Add (action);
		}
	}
	/// <summary>
	/// 取消 事件 从事件列表中移除添加的事件
	/// </summary>
	/// <param name="action">Action.</param>
	public void UnSubscribe(Action<T> action){
		if (_actions == null) {
			return;
		}
		if (_actions.Contains (action)) {
			_actions.Remove (action);
		}
	}
	/// <summary>
	/// 移除所有订阅 注册事件
	/// Clear this instance.
	/// </summary>
	public void ClearAll(){
		if (_actions == null) {
			return;
		}
		_actions.Clear ();
	}
}

