﻿/// <summary>
/// 全局可访问的事件容器
/// 
/// Event service.
/// </summary>
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
public class EventService 
{
	private readonly static EventService _instance = new EventService();

	/// <summary>
	/// 存放事件对象的集合
	/// The event bases.
	/// </summary>
	private readonly Dictionary<Type,EventBase> _eventBases = new Dictionary<Type,EventBase>();

	/// <summary>
	/// 返回单例实例对象
	/// </summary>
	/// <value>The instance.</value>
	public static EventService Instance{
		get {
			return _instance;
		}
	}
	/// <summary>
	/// 私有构造方法
	/// </summary>
	private EventService(){
	}

	/// <summary>
	/// 获取一个事件对象 ，该对象用于事件订阅中发布
	/// </summary>
	/// <returns>The event.</returns>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public T GetEvent<T>() where T :EventBase{
		Type eventType = typeof(T);
		if (!_eventBases.ContainsKey (eventType)) {
			//如果对象不存在，刚创建一个该类型对象
			T e = Activator.CreateInstance<T>();
			_eventBases.Add (eventType,e);
		}
		return (T)_eventBases[eventType];
	}
	/// <summary>
	/// 移除所有事件对象
	/// </summary>
	public void ClearAll(){
		foreach (EventBase e in _eventBases.Values){
			e.ClearAll();
		}
		_eventBases.Clear();
	}
	/// <summary>
	/// 移除所有KeepOnLevelChanging标记为false事件对象的订阅方法，并不移除该事件对象
	/// </summary>
	/// <param name="level">Level.</param>
	public void ClearOnLevelChanging(int level){
		foreach (EventBase e in _eventBases.Values) {
			e.ClearAll ();
		}
	}
}

