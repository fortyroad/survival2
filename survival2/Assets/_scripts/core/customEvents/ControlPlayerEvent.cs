﻿/// <summary>
/// 控制角色的自定义事件
/// Control player event.
/// </summary>
using UnityEngine;
using System.Collections;

public class ControlPlayerEvent : EventBaseArg<string>
{
	public readonly static string MoveLeft = "MoveLeft";
	public readonly static string MoveRight = "MoveRight";
	public readonly static string Stop = "Stop";
	public readonly static string Jump = "Jump";
	public readonly static string Fire = "Fire";
	public readonly static string Bobm = "Bomb";
	public readonly static string ChangeGun = "ChangeGun";
	public readonly static string ChangeBullet = "ChangeBullet";
}


