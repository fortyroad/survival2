﻿using UnityEngine;
using System.Collections;

public class GamePause : MonoBehaviour
{
	protected bool paused;
	void OnPauseGame ()
	{
		paused = true;
		Debug.Log ("onPauseGame----");
	}


	void OnResumeGame ()
	{
		paused = false;
	}


	void Update ()
	{
		if (!paused) {
			// do movement
		}
	}
}

