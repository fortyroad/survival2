﻿/// <summary>
/// Base enemy.
/// 
/// </summary>
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BaseEnemy :GamePause,IEnemy {
	#region 公共属性
	[Header("基本属性")]
	public float hp;//血值

	public float attackNum;//攻击力

	public float moveSpeed;//移动速度

	public Image hpBar;//血条

	public float attackRate;//攻击速度 单位秒 多少秒攻击一次

	[Header("用到的控制变量")]
	public string poolName;//缓存池名称 对象名字符串

	public bool is_moving = false;//是否在移动

	public bool is_attack = false;//是否在进行攻击

	public bool is_died = false;//是否已经死亡

	public bool is_stop = false;//停

	public string cur_direction = "";//当前的方向 left  right 

	public int moveVector = 1;// right:1 left:-1

	public BasePlayer player;//玩家对象




	public Transform hit_start;
	public Transform hit_end;
	#endregion

	#region 过程变量 
	public float curMoveSpeed;//当前移动速度 

	public float curHp;//当前血值

	private bool isCanBeAttack=false;

	private float startAttackTime;

	private float startDieTime;

	private bool startDie;
	#endregion

	#region 组件控制对象

	private Rigidbody2D rbody;//刚体对象

	private SpriteRenderer sprite;// 精灵对象  受伤时可改变精灵颜色

	private Animator animator;//动画对象


	#endregion
	#region 可被重写的方法
	//初始化
	public virtual void Awake(){
		rbody = GetComponent<Rigidbody2D> ();
		sprite = GetComponent<SpriteRenderer> ();
		animator = GetComponent<Animator> ();
		player = FindObjectOfType<BasePlayer> ();

	}

	// Use this for initialization
	public virtual void Start () {
		Reset ();
		Move ();
	}
	public virtual void OnEnable(){
		Reset ();
	}

	//Update is called once per frame
	public virtual void Update(){
		if (paused) {
			return;
		}
		if (transform.localScale.x > 0) {
			if (hpBar.transform.localScale.x < 0) {
				hpBar.transform.localScale *= -1;
			} 
		} else {
			if (hpBar.transform.localScale.x < 0) {
				hpBar.transform.localScale *= -1;
			}
		}
		float curPercent = curHp / hp;
		hpBar.rectTransform.localScale = new Vector3 (curPercent, 1, 1);


		GameObject go = CheckHitGameObj ();

		if (curHp > 0) {
			if (go != null) {
				isCanBeAttack = true;
				curMoveSpeed = 0f;
			} else {
				isCanBeAttack = false;
				curMoveSpeed = moveSpeed;
			}
			AttackPlayer ();
		}

		Move ();
		if (startDie&& Time.time - startDieTime > 1) {
			Anim_Die ();
		}
	}
	void AttackPlayer(){
		if (isCanBeAttack) {
			if (Time.time - startAttackTime > attackRate) {
				startAttackTime = Time.time;
				Anim_Attack();
			} 
		} 
	}
	#endregion

	#region 射线检测碰撞
	public GameObject CheckHitGameObj(){
		int mask = 1 << LayerMask.NameToLayer ("player");
		RaycastHit2D hit = Physics2D.Linecast (hit_start.position, hit_end.position, mask);
		if (hit.collider != null) {
			return hit.collider.gameObject;
		} else {
			return null;
		}
			
	}
	#endregion

	#region 实现 接口方法 
	public void BeAttack(float attNum){
		//被攻击
		if (curHp <= 0) {
			curMoveSpeed = 0f;
			startDie = true;
			return;
		} else {
			curHp -= attNum;
			if (curHp <= 0) {
				curMoveSpeed = 0f;
				curHp = 0;
				startDieTime = Time.time;
				startDie = true;
				Anim_Die ();
				Debug.Log ("curHp =="  + curHp);
			}
		}

		StartCoroutine ("BeAttackEffect");
	}

	public void BeSlowVelocity(){
		//被减速
	}

	public void Attack(){
		//业务逻辑攻击 攻南动画完成后对玩家造成伤害 
		if(player != null){
			Debug.Log ("对玩家造成伤害==" + attackNum);
			player.BeAttack (attackNum);
		}
	}

	public void Die(){
		//死亡
		SmartPool.Despawn(gameObject);
	}

	public void Move(){
		if (player == null) {
			curMoveSpeed = 0;
		}
		if (isCanBeAttack) {
			return;
		}
		//移动	
		SetMoveDircetion ();
		rbody.velocity = new Vector2 (curMoveSpeed * moveVector,rbody.velocity.y);
		transform.localScale = new Vector3(moveVector * Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
	}

	public void Reset(){
		//重置
		curHp = hp;
		curMoveSpeed = moveSpeed;
		hpBar.rectTransform.localScale = new Vector3(1,1,1);
		is_moving = true;
		is_attack = false;
		is_died = false;
		is_stop = false;
		isCanBeAttack = false;
		startDie = false;
		if (animator != null) {
			animator.SetBool ("is_moving",is_moving);
			animator.SetBool ("is_attack",is_attack);
			animator.SetBool ("is_died",is_died);
			animator.SetBool ("is_stop",is_stop);
		}

	} 

	#endregion
	//设置移动方向  向玩家的方向移动
	private void SetMoveDircetion(){
		if (player == null) {
			return;
		}
		if (Mathf.Abs (transform.position.x - player.transform.position.x) < 20) {
			curMoveSpeed = 0;
			Anim_Move ();
		}

		if (transform.position.x > player.transform.position.x) {
			if (cur_direction != "left") {
				cur_direction = "left";
				moveVector = -1;
			}
		} else {
			if (cur_direction != "right") {
				cur_direction = "right";
				moveVector = 1;
			}
		}
	}



	#region 动画控制方法 敌人有基本动画 停 走 攻击 死亡
	public void Anim_Move(){
		is_moving = true;
		is_attack = false;
		is_died = false;
		is_stop = false;
		if (animator != null) {
			animator.SetBool ("is_moving",is_moving);
			animator.SetBool ("is_attack",is_attack);
			animator.SetBool ("is_died",is_died);
			animator.SetBool ("is_stop",is_stop);
		}
	}
	public void Anim_Attack(){
		if (curHp <= 0) {
			return;
		}
		is_moving = false;
		is_attack = true;
		is_died = false;
		is_stop = false;
		if (animator != null) {
			animator.SetBool ("is_moving",is_moving);
			animator.SetBool ("is_attack",is_attack);
			animator.SetBool ("is_died",is_died);
			animator.SetBool ("is_stop",is_stop);
		}
//		Debug.Log ("Anim_Attack");
	}
	public void Anim_Die(){
		is_moving = false;
		is_attack = false;
		is_died = true;
		is_stop = false;
		if (animator != null) {
			animator.SetBool ("is_moving",is_moving);
			animator.SetBool ("is_attack",is_attack);
			animator.SetBool ("is_died",is_died);
			animator.SetBool ("is_stop",is_stop);
		}
	}
	#endregion

	#region 动画帧里的回调事件
	public void Anim_Call_Attack(){
		Attack ();
		is_moving = false;
		is_attack = false;
		is_died = false;
		is_stop = true;
		if (animator != null) {
			animator.SetBool ("is_moving",is_moving);
			animator.SetBool ("is_attack",is_attack);
			animator.SetBool ("is_died",is_died);
			animator.SetBool ("is_stop",is_stop);
		}
	}
	public void Anim_Call_Die(){
		//死亡动画完成完 移除对象到缓存池
		SmartPool.Despawn(gameObject);
	}
	#endregion


	#region 协程处理的方法
	/// <summary>
	/// 被攻击变色效果
	/// </summary>
	/// <returns>The attack effect.</returns>
	IEnumerator BeAttackEffect(){
		sprite.color = Color.red;	
		yield return new WaitForSeconds (0.1f);
		sprite.color = Color.white;
		StopCoroutine ("BeAttackEffect");
	}

	#endregion
}
