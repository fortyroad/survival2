﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
public class TouchMoveEventTrigger : MonoBehaviour,IPointerDownHandler,IPointerUpHandler,IPointerExitHandler {

	[SerializeField]
	public UnityEvent m_movePress = new UnityEvent();

	private Vector2 firstPos;
	private Vector2 sencondPos;
	private bool isPointDown = false;
	public float offsetY = 50f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

	//按下
	public void OnPointerDown(PointerEventData eventData){
		isPointDown = true;
		firstPos = eventData.position;
	} 
	public void OnPointerUp(PointerEventData eventData){
		isPointDown = false;
		sencondPos = eventData.position;
		float _offsetY = firstPos.y - sencondPos.y  ;
		if (_offsetY > offsetY) {
			m_movePress.Invoke ();
		}
	}
	public void OnPointerExit(PointerEventData eventData){
		isPointDown = false;
	}
}
