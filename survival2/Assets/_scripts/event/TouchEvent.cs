﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchEvent : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public bool SingletonTouch(){
		if (Input.touchCount == 1) {
			return true;
		}
		return false;
	}
	/// <summary>
	/// 判断单点触摸条件下 是否为移动触摸
	/// </summary>
	/// <returns><c>true</c>, if single touch was moved, <c>false</c> otherwise.</returns>
	public bool MoveSingleTouch(){
		if (Input.GetTouch (0).phase == TouchPhase.Moved) {
			return true;
		}
		return false;
	}
	 /// <summary>
	/// 判断两只手指 至少有一只为移动触摸
	 /// </summary>
	 /// <returns><c>true</c>, if multi touch was moved, <c>false</c> otherwise.</returns>
	public bool MoveMultiTouch(){
		if (Input.GetTouch (0).phase == TouchPhase.Moved || Input.GetTouch (1).phase == TouchPhase.Moved) {
			return true;
		}
		return false;
	}
	/// <summary>
	/// 判断手指移动的方向
	/// 向上1
	/// 向下-1
	/// </summary>
	/// <returns>The finger.</returns>
	public int JudueFinger(){
		if (Input.GetTouch (0).phase == TouchPhase.Began && startPosFlag == true) {
			startFingerPos = Input.GetTouch (0).position;
			startPosFlag = false;
		}
		if (Input.GetTouch (0).phase == TouchPhase.Ended) {
			startPosFlag = true;
		}
		nowFingerPos = Input.GetTouch (0).position;
		xMoveDistance = Mathf.Abs (nowFingerPos.x - startFingerPos.x);
		yMoveDistand = Mathf.Abs (nowFingerPos.y = startFingerPos.y);
		if (xMoveDistance > yMoveDistand) {
			if (nowFingerPos.x - startFingerPos.x > 0) {
				backValue = 1;	//x轴向方向  左
			} else {
				backValue = 2; //x轴正方向 右
			}
		} else {
			if (nowFingerPos.y - startFingerPos.y > 0) {
				backValue = 3; //y轴正方向 上
			} else {
				backValue = 4;//y轴负方向 下
			}
		}
		Debug.Log ("backValue ====== " + backValue);
		return backValue;
	}
	private bool startPosFlag;
	private Vector2 startFingerPos;
	private Vector2 nowFingerPos;
	private float xMoveDistance;
	private float yMoveDistand;
	private int backValue = 0;
}
