﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;
using System.IO;
using System;
using System.Text;
using LitJson;
static class CommonUtil {
	/*
	 * 
userData = new UserData ();
			List<UserWapon> userWapons = new List<UserWapon> ();
			UserWapon uwapon = new UserWapon ();
			uwapon.bulletNum = 1000;
			uwapon.name = "gun";
			uwapon.waponType = UserWapon.WaponType.Gun;
			uwapon.wapon_id = 11;
			userWapons.Add (uwapon);
			userData.name = "test";
			userData.userWapons = userWapons;
			userData.u_id = "uid";
		*/
	/// <summary>
	/// Saves the user data.
	/// </summary>
	/// <param name="userData">User data.</param>
	public static void SaveUserData(UserData userData){
		string filePath = Application.dataPath + "/_data/UserData.txt"; 
		FileInfo file = new FileInfo(filePath);  
		StreamWriter sw = file.CreateText();  
		string json = JsonMapper.ToJson(userData);  
		sw.WriteLine(json);  
		sw.Close();  
		sw.Dispose();  
//		#if UNITY_EDITOR
//		AssetDatabase.Refresh();
//		#endif
	}
	public static UserData LoadUserData(){
//		TextAsset ta = Resources.Load ("_data/UserData") as TextAsset;
//		Debug.Log ("-----------1");
//		if (!ta) {
//			Debug.Log ("-----------2");
//			return null;
//		}
//		string _str = ta.text;
//		UserData userData = JsonMapper.ToObject<UserData> (_str);
//		Debug.Log ("-----------3");
//		return userData;

		StreamReader sr = null;
		string str = "";
		try{
			sr = File.OpenText(Application.dataPath +"/_data/UserData.txt");
		}catch(Exception e){
			return null;
		}
		string line;
		while ((line = sr.ReadLine ()) != null) {
			str += line;
		}
		sr.Close ();
		sr.Dispose ();
		if (str.Trim ().Equals ("")) {
			return null;
		}
		UserData userData = JsonMapper.ToObject<UserData> (str);
		return userData;
	}


}

public class UserData{
	public string name;
	public string u_id;
	public List<UserWapon> userWapons;
}
public class UserWapon{
	public enum WaponType{
		Gun,	//枪
		Wapon,  //近战
		Bobm   //手雷
	}
	public int wapon_id;
	public string name;
	public int bulletNum;
	public WaponType waponType;
}