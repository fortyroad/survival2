﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
//[ExecuteInEditMode]
public class ChangeBar : GamePause{
	public Image processBar;//进度条
	public float changeTime;//更换时间 单位秒
	public bool changeOver;

	public Vector3 offset;
	#region 过程控制变量
	private float startTime;
	private Transform player;
	#endregion


	// Use this for initialization
	void Start () {
		player = PlayerController.Instance.transform;
		gameObject.SetActive (false);
//		Debug.Log ("gameObject.Active == "+gameObject.activeSelf);
//		ResetStatus ();
	}
	
	// Update is called once per frame
	void Update () {
		if (paused) {
			return;
		}
		transform.position = player.position + offset;
		ChangeProcessBar ();
	}
	void ChangeProcessBar(){
		float passTime = Time.time - startTime;
		if (passTime>0 && passTime < changeTime) {
			processBar.rectTransform.localScale = new Vector3 (passTime / changeTime, 1, 1);	
			changeOver = false;
		} 
		if(passTime > changeTime ){
			processBar.rectTransform.localScale = new Vector3 (1, 1, 1);
			changeOver = true;
			PlayerController.Instance.ChangeBulletAnimOver (); 
			gameObject.SetActive (false);
		}
	}
	internal void ResetStatus(){
		changeOver = false;
		processBar.rectTransform.localScale = new Vector3 (0, 1, 1);
		startTime = Time.time;
		gameObject.SetActive (true);
	}

}
