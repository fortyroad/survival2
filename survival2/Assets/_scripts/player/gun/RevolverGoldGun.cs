﻿using UnityEngine;
using System.Collections;

public class RevolverGoldGun : BaseGun,IGun
{
	public override	void Awake (){
		base.Awake ();
	}
	// Use this for initialization
	public override void Start () {
		base.Start ();
	}
	// Update is called once per frame
	public override void Update () {
		base.Update ();
	}

	//重置的方法  被击知需要重设置的变量
	public  override void Reset(){
		base.Reset ();
	}
	public void OnEnable(){
		Reset ();
	}
	#region 开枪ui 事件调用 moveVector 射击方向 角度
	public virtual void Fire(){
		if (bulletNum <= 0) {
			StopFire ();
			if (!is_change) {
				ChangeGun ();
			}

			return;
		}
		if (curClipNum <= 0) {
			StopFire ();
			if (!is_change) {
				ChangeBulletClip ();
			}
			return;
		}
		if (canbeFire) {
			BulletSigleGenerator ();
			startFireTime = Time.time;
		} 
	}

	public override void StopFire(){
		is_fire = false;
		transform.localScale = new Vector3 (1f, 1f, 1f);
	}
	#endregion

	private void BulletSigleGenerator(){
		curClipNum--;
		var bullet = SmartPool.Spawn (bulletPoolName);
		bullet.transform.position = bulletPos.transform.position;
		bullet.GetComponent<BaseBullet> ().SetLocalScale(gunVector) ;
		bullet.transform.rotation = transform.rotation;
		if (gunEffectPoolName!= null && !gunEffectPoolName.Trim().Equals("")) {
			var effect = SmartPool.Spawn (gunEffectPoolName);
			effect.transform.parent = transform.parent;
			effect.transform.localScale = transform.localScale;
			effect.transform.rotation = transform.rotation;
			effect.transform.position = effectPos.transform.position;
		}
		if (bulletDropPoolName != null&& !bulletDropPoolName.Trim().Equals("")) {
			var go = SmartPool.Spawn (bulletDropPoolName);
			go.transform.position = dropPos.transform.position;
		}

		if (Mathf.Abs (transform.localScale.x) == 1) {
			StartCoroutine ("ShowGunJump");
		}
	}

	#region 没有子弹 更换弹夹  更换枪  这里应该使用一个协程方法
	private void ChangeBulletClip(){
		if (is_change) {
			return;
		}
		is_change = true;
		PlayerController.Instance.changeBar.GetComponent<ChangeBar> ().changeTime = changeBulletTime;
		PlayerController.Instance.changeBar.GetComponent<ChangeBar> ().ResetStatus ();
	}

	private void ChangeGun(){
		if (is_change) {
			return;
		}
		is_change = true;

	}

	internal void ChangeBulletAnimOver(){
		if (!is_change) {
			return;
		}
		float leftNum = bulletNum - clipNum + curClipNum;
		if (leftNum > 0) {
			curClipNum = clipNum;
		} else {
			curClipNum = bulletNum;
		}
		bulletNum -= curClipNum;
		is_change = false;

	}
	#endregion

	IEnumerator ShowGunJump(){
		transform.localScale = new Vector3 (transform.localScale.x*0.95f,1f,1f);
		yield return new WaitForSeconds (0.1f);
		transform.localScale = new Vector3 (1f, 1f, 1f);
		StopCoroutine ("ShowGunJump");
	}
}

