﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[ExecuteInEditMode]
public class Rador : MonoBehaviour {

	PlayerController container;
	void Awake(){
		container = this.transform.parent.GetComponent<PlayerController> ();
	}
	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
	
	}
	void OnEnable(){
		if (PlayerController.Instance.enemies.Count > 0) {
			PlayerController.Instance.enemies= new List<GameObject> ();
		}
	}
	void OnDisable(){
		if (PlayerController.Instance.enemies.Count > 0) {
			PlayerController.Instance.enemies= new List<GameObject> ();
		}
	}

	public void OnTriggerEnter2D(Collider2D coll){
		var tag = coll.gameObject.tag;
		if (tag == "enemy") {
			container.AddEnemyInRange (coll.gameObject);
		}
	}

	public void OnTriggerExit2D(Collider2D coll){
		var tag = coll.gameObject.tag;
		if (tag == "enemy") {
			container.RemoveEnemyInRange (coll.gameObject);
		}
	}
}
