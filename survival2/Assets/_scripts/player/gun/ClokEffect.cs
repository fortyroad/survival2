﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClokEffect : GamePause{
	public string poolName;
	public float liveTime;
	private float startTime;
	public Sprite[] sps;
	private SpriteRenderer sp;
	void Awake(){
		sp = GetComponent<SpriteRenderer> ();
//		sp.sprite
	}
	// Use this for initialization
	void Start () {
		startTime = Time.time;
		sp.sprite=sps[Random.Range (0, sps.Length)];
	}
	
	// Update is called once per frame
	void Update () {
		if (paused) {
			return;
		}
		if (Time.time - startTime > liveTime) {
			SmartPool.Despawn (gameObject);
		}
	}

}
