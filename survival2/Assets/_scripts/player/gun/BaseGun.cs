﻿/// <summary>
/// 远程武器枪械的基类 
/// </summary>
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseGun : GamePause {

	#region 基本属性 
	public int wapon_id;
	public string name;//枪名子
	public string bulletPoolName;//子弹缓存池名
	public string gunEffectPoolName;//开枪效果使用的对象缓存名
	public string bulletDropPoolName;//弹壳缓存池名
	public string poolName;//缓存池名
	public float fireRate;//发射速率
	public float bulletNum;//子弹总数量
	public float clipNum;//弹夹子弹数量
	public float changeBulletTime;

	public Transform bulletPos;
	public Transform effectPos;
	public Transform dropPos;

	#endregion

	#region 过程控制变量

	public bool is_fire = false;
	public bool is_change = false;
	public int gunVector = 1; //right:-1 left:1
	public float curClipNum;//当前弹夹子弹数量
	public GameObject curObj;//自动瞄准乱放对象

	public GameObject arraw;
	public PlayerController pc;//引用角色位置
	#endregion 

	//子弹射击的控制变量
	public float startFireTime;
	public bool canbeFire;
	public virtual	void Awake (){
	}
	// Use this for initialization
	public virtual void Start () {
		Reset ();
		pc = FindObjectOfType<PlayerController> ();
		if (clipNum <= 0 && bulletNum > 0) {
			ChangeBulletClip ();
		}
		if (bulletNum <= 0) {
			ChangeGun ();
		}
	}
	// Update is called once per frame
	public virtual void Update () {
		if (paused) {
			return;
		}
		if (Time.time - startFireTime > fireRate) {	
			canbeFire = true;
			if (arraw) {
				arraw.SetActive (true);
			}
		} else {
			canbeFire = false;
			if (arraw) {
				arraw.SetActive (false);
			}
		}
		if (pc != null) {
			gunVector = pc.moveVector;
		}
		if (curObj != null) {
			Vector3 targetDir = curObj.transform.position - transform.TransformPoint (this.transform.localPosition);
			float angle = Mathf.Atan2 (targetDir.y, targetDir.x) * Mathf.Rad2Deg;
			if (targetDir.y > 0 && targetDir.x > 0) {
				//1
			} else if (targetDir.y >0 && targetDir.x < 0) {
				//2
				angle -= 180;
			} else if (targetDir.y < 0 && targetDir.x < 0) {
				//3
				angle =180 + angle;
				
			} else if (targetDir.y < 0 && targetDir.x > 0) {
				//4
			} else {
				angle = 0;
			}
			PlayerController.Instance.head.transform.rotation = Quaternion.Euler (0, 0, angle/2);

			transform.rotation = Quaternion.Euler (0, 0, angle);


		} else {
			transform.rotation = Quaternion.Euler (0, 0, 0);
			PlayerController.Instance.head.transform.rotation = Quaternion.Euler (0, 0, 0);
		}
	}

	//重置的方法  被击知需要重设置的变量
	public virtual void Reset(){
		is_fire = false;
		startFireTime = Time.time;
	}
	public void OnEnable(){
		Reset ();
	}
	#region 开枪ui 事件调用 moveVector 射击方向 角度
	public virtual void Fire(){
		if (bulletNum <= 0) {
			StopFire ();
			if (!is_change) {
				ChangeGun ();
			}

			return;
		}
		if (curClipNum <= 0) {
			StopFire ();
			if (!is_change) {
				ChangeBulletClip ();
			}
			return;
		}
		if (canbeFire) {
			BulletSigleGenerator ();
			startFireTime = Time.time;
		} 
	}
//	public void GoOnFire(){
//		if (bulletNum <= 0) {
//			StopFire ();
//			if (!is_change) {
//				ChangeGun ();
//			}
//
//			return;
//		}
//		if (curClipNum <= 0) {
//			StopFire ();
//			if (!is_change) {
//				ChangeBulletClip ();
//			}
//			return;
//		}
//		if (is_fire && curClipNum > 0) {
//			return;
//		}
//
//		is_fire = true;
//		StartCoroutine ("BulletGenerator");
//	}
	public virtual void StopFire(){
		is_fire = false;
		transform.localScale = new Vector3 (1f, 1f, 1f);

	}
		
	#endregion



//	#region 协程方法 
//	IEnumerator BulletGenerator(){
//		if (is_fire&&curClipNum > 0){
//			curClipNum--;
//			var bullet = SmartPool.Spawn (bulletPoolName);
//			bullet.transform.position = new Vector3(transform.position.x,transform.position.y,-1);
//			bullet.GetComponent<BaseBullet> ().moveVector = gunVector;
//			bullet.transform.rotation = transform.rotation;
//			if (anim != null) {
//				anim.SetBool ("is_fire", true);
//			}
//		}
//		yield return new WaitForSeconds (fireRate);
//		if (curClipNum <= 0) {
//			StopCoroutine ("BulletGenerator");
//			StopFire ();
//			ChangeBulletClip ();
//		} else {
//			StartCoroutine ("BulletGenerator");
//		}
//
//	}
//	#endregion

	private void BulletSigleGenerator(){
		curClipNum--;
		var bullet = SmartPool.Spawn (bulletPoolName);
		bullet.transform.position = bulletPos.transform.position;
		bullet.GetComponent<BaseBullet> ().SetLocalScale(gunVector) ;
		bullet.transform.rotation = transform.rotation;
		bullet.SetActive (true);
		if (gunEffectPoolName!= null && !gunEffectPoolName.Trim().Equals("")) {
			var effect = SmartPool.Spawn (gunEffectPoolName);
			effect.transform.parent = transform.parent;
			effect.transform.localScale = transform.localScale;
			effect.transform.rotation = transform.rotation;
			effect.transform.position = effectPos.transform.position;
		}
		if (bulletDropPoolName != null&& !bulletDropPoolName.Trim().Equals("")) {
			var go = SmartPool.Spawn (bulletDropPoolName);
			go.transform.position = dropPos.transform.position;
		}

		if (Mathf.Abs (transform.localScale.x) == 1) {
			StartCoroutine ("ShowGunJump");
		}
	}

	public void FireAnimOver(){
		
	}


	#region 没有子弹 更换弹夹  更换枪  这里应该使用一个协程方法
	private void ChangeBulletClip(){
		if (is_change) {
			return;
		}
		is_change = true;
		PlayerController.Instance.changeBar.GetComponent<ChangeBar> ().changeTime = changeBulletTime;
		PlayerController.Instance.changeBar.GetComponent<ChangeBar> ().ResetStatus ();
	}

	private void ChangeGun(){
		if (is_change) {
			return;
		}
		is_change = true;

	}

	internal void ChangeBulletAnimOver(){
		if (!is_change) {
			return;
		}
		float leftNum = bulletNum - clipNum + curClipNum;
		if (leftNum > 0) {
			curClipNum = clipNum;
		} else {
			curClipNum = bulletNum;
		}
		bulletNum -= curClipNum;
		is_change = false;
//		GameManager.Instance.SubGunBullet (bulletNum);
//		UIController.Instance.ShowBulletNum();
	}
	#endregion

	IEnumerator ShowGunJump(){
		transform.localScale = new Vector3 (transform.localScale.x*0.95f,1f,1f);
//		float _z = -5*gunVector;
//		transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, transform.rotation.z + _z);
		yield return new WaitForSeconds (0.1f);
		transform.localScale = new Vector3 (1f, 1f, 1f);
//		transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, transform.rotation.z - _z);
		StopCoroutine ("ShowGunJump");
	}

}
