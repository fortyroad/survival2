﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBobm : GamePause {
	#region 基本属性
	public float attackNum;//攻击力
	public float durationTime;//多长时间后爆炸
	public string poolName;
	public Vector2 force;//扔出去的力
	public float bombRadius;//半径
	private float startTime;
	#endregion
	public bool isBobmed ;

	private Rigidbody2D body;

	void OnEnable(){
		isBobmed = false;
		body = GetComponent<Rigidbody2D> ();
	}
	// Use this for initialization
	public void Throw(int moveVector){
		startTime = Time.time;
		body.velocity = new Vector2 (force.x * moveVector, force.y);
		transform.RotateTowardsOverTime (new Vector3 (0, 0, -360), durationTime / 3);
	}
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (paused) {
			return;
		}
		if (isBobmed) {
			return;
		}

		if (Time.time - startTime > durationTime) {
			isBobmed = true;
			body.velocity = Vector2.zero;
			Bobmed ();
		}
	}
	public virtual void  Bobmed(){
		
		int mask = 1 << LayerMask.NameToLayer ("enemy");
		Collider2D[] others = Physics2D.OverlapCircleAll(transform.position,bombRadius,mask);//获取所有碰撞体
		Debug.Log("Bobmed=======1==="+others.Length  + " ==" + transform.position.x);
		for(int i = 0 ;i < others.Length;i++){
			GameObject coll = others [i].gameObject;
			if (coll.tag.Equals ("enemy")) {
				if (coll.gameObject.GetComponent<BaseEnemy> ().curHp > 0) {
					coll.gameObject.GetComponent<BaseEnemy> ().BeAttack (attackNum);
				}
			}
		}
		Recycle ();
	}
	void Recycle(){
		Destroy(gameObject);
//		SmartPool.Despawn (gameObject);
	}
}
