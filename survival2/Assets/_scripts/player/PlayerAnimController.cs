﻿using UnityEngine;
using System.Collections;
public class PlayerAnimController :GamePause {
	#region  动画控制变量
	public int wapon_id; //武器id 无武器0   区分近战（1-9） 远程（10-20）>10
	public bool is_moving;//移动
	public bool is_stop; //站立
	public bool is_attack;//攻击
	public bool is_jump;//起跳
	public bool is_fly;//跳跃过程中
	public bool is_roll;//着地滚动
	public bool is_die;//死亡状态
	public bool is_hurt;//受伤
	public bool is_throw;//扔雷
	#endregion
	private Animator anim;
	void Awake(){
		anim = GetComponent<Animator> ();
	}
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (paused) {
			return;
		}
		anim.SetInteger ("wapon_id",wapon_id);
		anim.SetBool ("is_moving", is_moving);
		anim.SetBool ("is_stop", is_stop);
		anim.SetBool ("is_attack", is_attack);
		anim.SetBool ("is_jump", is_jump);
		anim.SetBool ("is_fly", is_fly);
		anim.SetBool ("is_roll", is_roll);
		anim.SetBool ("is_die", is_die);
		anim.SetBool ("is_hurt", is_hurt);
		anim.SetBool ("is_throw",is_throw);
	}
	//动画状态有 走（跑），站立，攻击，起跳，空中，着地滚动,死亡，受伤
	//	走（跑）
	public void Move(int wid){
		//走（跑）
		wapon_id = wid;
		is_moving = true;
		is_stop = false;
		is_attack = false;
		is_jump = false;
		is_fly = false;
		is_roll = false;
		is_die = false;
		is_hurt = false;
		is_throw = false;
	}
	//站立
	public void Stop(int wid){
		//站立
		wapon_id = wid;
		is_moving = false;
		is_stop = true;
		is_attack = false;
		is_jump = false;
		is_fly = false;
		is_roll = false;
		is_die = false;
		is_hurt = false;
		is_throw = false;
	}
	//近战武器攻击
	public void Attack (int wid){
		//近战武器攻击
		wapon_id = wid;
		is_moving = false;
		is_stop = false;
		is_attack = true;
		is_jump = false;
		is_fly = false;
		is_roll = false;
		is_die = false;
		is_hurt = false;
		is_throw = false;
	}
	//起跳
	public void Jump(int wid){
		//起跳
		wapon_id = wid;
		is_moving = false;
		is_stop = false;
		is_attack = false;
		is_jump = true;
		is_fly = false;
		is_roll = false;
		is_die = false;
		is_hurt = false;
		is_throw = false;
	}
	//起跳后空中状态
	public void Fly(int wid){
		//起跳后空中状态
		wapon_id = wid;
		is_moving = false;
		is_stop = false;
		is_attack = false;
		is_jump = false;
		is_fly = true;
		is_roll = false;
		is_die = false;
		is_hurt = false;
		is_throw = false;
	}
	//着地滚动
	public void Roll(int wid){
		//着地滚动
		wapon_id = wid;
		is_moving = false;
		is_stop = false;
		is_attack = false;
		is_jump = false;
		is_fly = false;
		is_roll = true;
		is_die = false;
		is_hurt = false;
		is_throw = false;
	}

	//受击受伤
	public void Hurt(int wid){
		//受击受伤
		wapon_id = wid;
		is_moving = false;
		is_stop = false;
		is_attack = false;
		is_jump = false;
		is_fly = false;
		is_roll = false;
		is_die = false;
		is_hurt = true;
		is_throw = false;
	}
	//死亡
	public void Die(int wid){
		wapon_id = 0;
		is_moving = false;
		is_stop = false;
		is_attack = false;
		is_jump = false;
		is_fly = false;
		is_roll = false;
		is_die = true;
		is_hurt = false;
		is_throw = false;
	}

	public void Throw(int wid,bool isFly){
		wapon_id = 0;
		is_moving = false;
		is_stop = false;
		is_attack = false;
		is_jump = false;
		is_fly = isFly;
		is_roll = false;
		is_die = false;
		is_hurt = false;
		is_throw = true;
	}

}
