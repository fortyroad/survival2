﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class BasePlayer : GamePause{
	#region 公共基本属性
	public float hp;//血值 

	public float moveSpeed;//移动速度

	public float jumpHeiht;

	#endregion

	#region  变量控制
	public float curSpeed;//当前移动速度

	public float curHp;//当前hp值

	public Image hpBar;
	#endregion 

	#region  
	public void BeAttack(float attNum){
		curHp -= attNum;
		if (curHp <= 0) {
			PlayerController.Instance.Be_die ();
		} else {
			PlayerController.Instance.Be_hurtAnim ();
		}
	}

	#endregion
	void Awake(){
		gameObject.tag ="player";
		curHp = hp;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		if (paused) {
			return;
		}
		if (transform.localScale.x > 0) {
			if (hpBar.transform.localScale.x < 0) {
				hpBar.transform.localScale *= -1;
			} 
		} else {
			if (hpBar.transform.localScale.x < 0) {
				hpBar.transform.localScale *= -1;
			}
		}
		float curPercent = curHp / hp;
		if (curPercent >= 0) {
			hpBar.rectTransform.localScale = new Vector3 (curPercent, 1, 1);
		}

	}
}
