﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : Singleton<PlayerController> {
	#region 公共属性 
	[Header("用到的控制变量")]
	/// <summary>
	/// 移动控制
	/// </summary>
	public bool is_attack = false;//是否在进行攻击
	public bool is_died = false;//是否已经死亡
	public bool is_jump = false;//跳跃
	public bool is_bobm = false;//扔雷
	public bool is_fly = false;//空中状态
	public bool is_roll = false;//着地滚动状态 不可攻击 不可开枪 不可扔雷

	private bool jumpping = false;
	public bool is_changeBullet = false;//换子弹
	public string cur_direction = "";//当前的方向 left  right 

	public int moveVector = 1;// right:-1 left:1

	#endregion
	public int curWaponId;
	public int waponIdx = 0;
	private GameObject curWapon;
	#region 玩家子对象 枪 近战武器
	public GameObject prefab_changBar;//引用对象
	public GameObject changeBar;//换子弹条
	public GameObject gun ;
	public GameObject wapon ;
//
//	public GameObject playerGun = null;
//	public GameObject playerWapon = null;
	public List<int> waponId;
	[Header("可带的武器1近战 2小枪 3大枪  ")]
	public List<GameObject> wapons;//
	[Header("头部")]
	public GameObject head;
	public SpriteRenderer headSp;

	[Header("起跳动画，位置")]
	public Transform jump_pos;
	public string jumpEffectPoolname;

	public GameObject enemysArear;

	[Header("雷的起点位置")]
	public Transform bobm_pos;
	#endregion
	#region 组件控制对象
	private Rigidbody2D rbody;
	private BasePlayer basePlayer;
	private PlayerAnimController animController;//
	#endregion 

	public SpriteRenderer sp1;
	public SpriteRenderer sp2;
	public SpriteRenderer sp3;


	public List<GameObject> enemies;

	public GameObject objBobm;
	private void ControllPlayerAction(string arg){
		if (arg.Equals (ControlPlayerEvent.MoveLeft)) {
			Move_Left ();
		} else if (arg.Equals (ControlPlayerEvent.MoveRight)) {
			Move_Right ();
		} else if (arg.Equals (ControlPlayerEvent.Stop)) {
			Move_Stop ();
		}else if (arg.Equals (ControlPlayerEvent.Jump)) {
			Player_Jump ();
		}else if(arg.Equals(ControlPlayerEvent.Fire)){
			Player_Fire ();
		}else if(arg.Equals(ControlPlayerEvent.Bobm)){
			Player_Bobm ();
		}else if(arg.Equals(ControlPlayerEvent.ChangeBullet)){
			Player_ChangeBullet ();
		}else if(arg.Equals(ControlPlayerEvent.ChangeGun)){
			ChangeGun ();
		}
	}

	/// <summary>
	/// Awake this instance.
	/// 初始化
	/// </summary>
	void Awake(){
		base.Awake ();

		EventService.Instance.GetEvent<ControlPlayerEvent>().Subscribe(ControllPlayerAction);
		rbody = GetComponent<Rigidbody2D> ();
		basePlayer = GetComponent<BasePlayer> ();
		animController = GetComponent<PlayerAnimController> ();
		enemies = new List<GameObject> ();
		changeBar = Instantiate (prefab_changBar);
	
		curWapon = Instantiate (wapons [waponIdx]);
//		curWapon = wapons [0];
		if (curWapon.tag.Equals ("gun")) {
			curWaponId = curWapon.GetComponent<BaseGun> ().wapon_id;
			//显示头部
			head.SetActive(true);
			enemysArear.SetActive (true);
			if (moveVector > 0) {
				curWapon.transform.localScale = new Vector3 (moveVector*transform.localScale.x,transform.localScale.y,transform.localScale.z);
			}
			curWapon.transform.position = gun.transform.position;
			curWapon.transform.parent = gun.transform;

		}
		if (curWapon.tag.Equals ("wapon")) {
			curWaponId = curWapon.GetComponent<BaseWapon>().wapon_id;
			head.SetActive (false);
			enemysArear.SetActive (false);

			curWapon.transform.position = wapon.transform.position;
			curWapon.transform.parent = wapon.transform;
		}
	
	}

	//换枪 换子弹时禁止换枪
	public void ChangeGun(){
		Debug.Log ("changeGun +" + curWapon.name);
		if (is_changeBullet) {
			return;
		}
		//切换武器 主枪 副枪 近战  启用玩家攻击敌人荡围
		if (waponIdx < wapons.Count - 1) {
			waponIdx++;
		} else {
			waponIdx = 0;
		}
		if (curWapon.GetComponent<BaseGun> ()) {
			Destroy (curWapon);
		}
		curWapon = Instantiate (wapons [waponIdx]);
		//		curWapon = wapons [0];
		if (curWapon.tag.Equals ("gun")) {
			curWaponId = curWapon.GetComponent<BaseGun> ().wapon_id;
			//显示头部

			head.SetActive(true);
			enemysArear.SetActive (true);
			if (moveVector > 0) {
				curWapon.transform.localScale = new Vector3 (moveVector*transform.localScale.x,transform.localScale.y,transform.localScale.z);
			}
			curWapon.transform.position = gun.transform.position;
			curWapon.transform.parent = gun.transform;

		}
		if (curWapon.tag.Equals ("wapon")) {
			curWaponId = curWapon.GetComponent<BaseWapon>().wapon_id;
			head.SetActive (false);
			enemysArear.SetActive (false);


			curWapon.transform.position = wapon.transform.position;
			curWapon.transform.parent = wapon.transform;
		}

	}

	void Update(){
		for (int i = 0; i < enemies.Count; i++) {
			if (enemies [i].GetComponent<BaseEnemy> ().curHp <= 0) {
				RemoveEnemyInRange (enemies [i]);
			}
		}
		if (enemies != null && enemies.Count > 0) {

			GameObject go = enemies [0];
				if (curWaponId >10 ) {
				if (curWapon.GetComponent<BaseGun> ().curObj == null) {
					curWapon.GetComponent<BaseGun> ().curObj = go;
					}
				}
		} else {
			if (curWaponId >10) {
				curWapon.GetComponent<BaseGun> ().curObj = null;
			}
		}
		Move ();	
	}

	// Use this for initialization
	void Start () {
//		playerGun = SmartPool.Spawn ("mp5Gun");
//		playerGun = Instantiate(p_gun);
//		playerGun.transform.parent = gameObject.transform;
//		playerGun.transform.position = gun.transform.position;
	}

	/// <summary>
	/// 玩家移动速度和方向的实现
	/// </summary>
	void Move(){
		rbody.velocity = new Vector2 (basePlayer.curSpeed * moveVector, rbody.velocity.y);
		transform.localScale = new Vector3(-moveVector * Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);

	}


	#region 控制玩家移动的几个调用方法
	public void Move_Left(){
		cur_direction = "left";
		moveVector = 1;
		basePlayer.curSpeed = basePlayer.moveSpeed;
		if (!is_jump && is_attack ) {
			basePlayer.curSpeed = 0;
		}
		if (is_bobm) {
			if (!is_fly) {
				basePlayer.curSpeed = 0;
			}
		}
		if (is_jump||is_roll||is_attack || is_bobm) {
			return;
		}
		animController.Move (curWaponId);

		
	}

	public void Move_Right(){
		cur_direction = "right";
		moveVector = -1;
		basePlayer.curSpeed = basePlayer.moveSpeed;
		if (!is_jump && is_attack) {
			basePlayer.curSpeed = 0;
		}
		if (is_bobm) {
			if (!is_fly) {
				basePlayer.curSpeed = 0;
			}
		}
		if (is_jump||is_roll||is_attack || is_bobm) {
			return;
		}
		animController.Move (curWaponId);

	}
	//站立
	public void Move_Stop(){
		basePlayer.curSpeed = 0f;
		if (is_attack||is_roll||is_fly||is_bobm) {
			return;
		}
		if (!is_jump) {
			animController.Stop (curWaponId);
		}
	}

	public void Be_hurtAnim(){
		StartCoroutine ("BeAttackEffect");
	}
	/*弃用
	public void Be_hurtAnimOver(){
	}*/
	public void Be_die(){
		EventService.Instance.GetEvent<ControlPlayerEvent> ().UnSubscribe (ControllPlayerAction);
		animController.Die (curWaponId);
		DropGun ();
	}
	private void DropGun(){
	
	}
	#endregion

	#region ui调用的几个方法
	//跳
	public void Player_Jump(){
		if (is_roll||is_attack) {
			return;
		}
		if (is_jump) {
			if (curWaponId > 10) {
				head.SetActive (true);
				gun.SetActive (true);
			} 
			return;
		}
		is_jump = true;
		rbody.velocity = new Vector2 (basePlayer.curSpeed, basePlayer.jumpHeiht);
		animController.Jump (curWaponId);
		if (jumpEffectPoolname != null) {
			var go = SmartPool.Spawn (jumpEffectPoolname);
			go.transform.position = jump_pos.transform.position;
		}
	}
	//开枪
	public void Player_Fire(){
		if (is_roll||is_changeBullet) {
			return;
		}
		//调用子对象gun 开枪事件 攻击事件
		if (curWaponId > 0 && curWaponId < 10) {
			bool _animStatus = curWapon.GetComponent<BaseWapon>().AttackEnemyAnim();
			if (_animStatus) {
				is_fly = false;
				is_roll = false;
				is_attack = true;
				basePlayer.curSpeed = 0;
				animController.Attack (curWaponId);
			}
		} else if (curWaponId > 10) {
			if(curWapon.GetComponent<BaseGun>()){
//				curWapon.GetComponent<BaseGun>().Fire();
				curWapon.GetComponent<IGun> ().Fire ();
			}

		}

	}
	public void Player_StopFire(){
		if (curWaponId > 0 && curWaponId < 10) {

		} else if (curWaponId > 10) {
			if(curWapon.GetComponent<BaseGun>()){
				curWapon.GetComponent<BaseGun>().StopFire ();
			}

		}

	}
	//扔雷
	public void Player_Bobm(){
		if (is_jump==true && is_fly==false) {
			return;
		}

		if (is_bobm) {
			return;
		}
		is_bobm = true;
		if (!is_fly) {
			basePlayer.curSpeed = 0;
		}
		animController.Throw (curWaponId, is_fly);
		if (curWaponId > 10) {
			head.SetActive (false);
			gun.SetActive (false);
		} 

	}
	//换弹
	public void Player_ChangeBullet(){
		if(!curWapon.GetComponent<BaseGun> ()){
			return;
		}
		if ( curWapon.GetComponent<BaseGun> ().curClipNum == curWapon.GetComponent<BaseGun> ().clipNum) {
			return;
		}
		if (is_changeBullet) {
			return;
		}
		is_changeBullet = true;
		curWapon.GetComponent<BaseGun> ().is_change = true;
		changeBar.GetComponent<ChangeBar> ().changeTime = curWapon.GetComponent<BaseGun>().changeBulletTime;
		changeBar.GetComponent<ChangeBar> ().ResetStatus ();
	}
	//近战武器攻击
	public void Player_Attack(){
		if (is_attack) {
			return;
		}
		is_attack = true;
	}
	#endregion


	#region 动画回调事件
	public void JumpOver(){
		//跳跃完成 转至行走状态
		if(is_jump){
			is_fly = true;
			animController.Fly (curWaponId);
		}

	}

	public void RollOver(){
		Debug.Log ("==============RollOver");
		if (curWaponId > 10) {
			head.SetActive (true);
			gun.SetActive (true);
		} 

		if (is_roll) {
			if (basePlayer.curSpeed == 0) {
				animController.Stop (curWaponId);
			} else {
				animController.Move (curWaponId);
			}
		}

		is_roll = false;
		is_jump = false;
	}
	//近战攻击
	public void AttackOver(){
		Debug.Log ("AttackOver");
		is_attack = false;
		if (basePlayer.curSpeed == 0) {
			animController.Stop (curWaponId);
		} else {
			animController.Move (curWaponId);
		}
		basePlayer.curSpeed = basePlayer.moveSpeed;
		if(curWapon.GetComponent<BaseWapon> ()){
			curWapon.GetComponent<BaseWapon> ().AttackEneyHurt ();
		}

	}
	//扔雷动作完成掉用
	public void ThrowOver(){
		Debug.Log ("throwOver");

		GameObject _bobm = Instantiate (objBobm);
	
		_bobm.GetComponent<BaseBobm>().Throw(moveVector);
		_bobm.transform.position = bobm_pos.transform.position;


		is_bobm = false;
		//生成一颗手雷 扔出去
		if (is_fly) {
			animController.Fly (curWaponId);
		} else {
			if (basePlayer.curSpeed == 0) {
				animController.Stop (curWaponId);
			} else {
				animController.Move (curWaponId);
			}
		}
		if (curWaponId > 10) {
			head.SetActive (true);
			gun.SetActive (true);
		} 
	}
	//死亡动画回调
	public void DieAnimOver(){
		Destroy (gameObject);
	}
	#endregion

	#region 碰撞检测是否在地面上
	void OnCollisionEnter2D(Collision2D col){
		Debug.Log ("----------" + col.gameObject.tag);
		if (col.gameObject.tag == "ground") {
			if (is_roll) {
				return;
			}
//			if (is_bobm) {
//				is_jump = false;
//				is_fly = false;
//				return;
//			}
			if (is_attack ||is_bobm) {
				is_jump = false;
				is_fly = false;

				return;
			}
			if (basePlayer.curSpeed == 0) {
				animController.Stop (curWaponId);
				Debug.Log ("----------------------1");
			}else{
				if (is_fly) {
					is_roll = true;
					if (curWaponId > 10) {
						head.SetActive (false);
						gun.SetActive (false);
					}
					Debug.Log ("----------------------2");
					animController.Roll (curWaponId);
				}else if(is_roll){
					Debug.Log ("----------------------3");
					RollOver ();
				} 
			}
			is_jump = false;
			is_fly = false;
		}

	}
	#endregion
	/// <summary>
	/// 自动瞄准 区域敌人对象的添加
	/// </summary>
	/// <param name="gameobj">Gameobj.</param>
	public void AddEnemyInRange(GameObject gameobj){
		if (enemies == null) {
			enemies = new List<GameObject> ();
		}
		if(gameobj.GetComponent<BaseEnemy>().curHp>0&&!enemies.Exists(c => c.gameObject == gameobj)){
			enemies.Add (gameobj);
		}
	}
	/// <summary>
	/// 自动瞄准 区域敌人对象的移除.
	/// </summary>
	/// <param name="gameobj">Gameobj.</param>
	public void RemoveEnemyInRange(GameObject gameobj){
		if (enemies != null && enemies.Exists (c => c.gameObject == gameobj)) {
			if (curWapon.GetComponent<BaseGun> ()) {
				if (curWapon.GetComponent<BaseGun> ().curObj != null && gameobj == curWapon.GetComponent<BaseGun> ().curObj) {
					curWapon.GetComponent<BaseGun> ().curObj = null;
				}
				enemies.Remove (gameobj);
			}

		}
	}


	#region
	//更换子弹结束
	internal void ChangeBulletAnimOver(){
		is_changeBullet = false;
		if (curWapon.GetComponent<BaseGun> ()) {
			curWapon.GetComponent<BaseGun> ().ChangeBulletAnimOver ();
		}

	}
	#endregion


	//主角受到攻击的效果
	IEnumerator BeAttackEffect(){
		Debug.Log ("BeAttackEffect---");
		sp1.color = Color.red;	
		if (head != null) {
			headSp.color = Color.red;
		}
		if(curWapon.GetComponent<SpriteRenderer> ()){

			curWapon.GetComponent<SpriteRenderer> ().color = Color.red;
		}

		yield return new WaitForSeconds (0.1f);
		sp1.color = Color.white;
		if (head != null) {
			headSp.color = Color.white;
		}
		if (curWapon.GetComponent<SpriteRenderer> ()) {
			curWapon.GetComponent<SpriteRenderer> ().color = Color.white;
		}

		StopCoroutine ("BeAttackEffect");
	}
}
