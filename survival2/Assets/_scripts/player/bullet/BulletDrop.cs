﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDrop : GamePause {
	public float liveTime;
	public Vector2 force;
	private float startTime;
	// Use this for initialization
	void OnEnable(){
		startTime = Time.time;
		transform.rotation =Quaternion.Euler (0, 0,Random.Range(-180,0));
		GetComponent<Rigidbody2D>().velocity = force;
	}
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (paused) {
			return;
		}
		if (Time.time - startTime > liveTime) {
			Recyle ();
		}
	}
	void Recyle(){
		SmartPool.Despawn (gameObject);
	}
}
