﻿using UnityEngine;
using System.Collections;

public class BaseBullet : GamePause{
	#region 公共属性
	[Header("基本属性")]
	public float attackNum;//攻击力 伤害值
	public float moveSpeed;//速度
	public float durationTime;//存活时间
	public string poolName;//缓存名
	public string diepoolName;//子弹消失的粒子效果
	[Header("用到的控制变量")]
//	public bool is_attack = false;//	是否在进行攻击
	private float startTime;
	public int moveVector = 1;// right:1 left:-1

	#endregion
	private Rigidbody2D body;
	public	virtual void  Awake(){
		startTime = Time.time;
		body = GetComponent<Rigidbody2D> ();
	}
	// Use this for initialization
	public virtual void  Start () {
	}
	// Update is called once per frame
	public virtual void  Update () {
		if (paused) {
			return;
		}

		if (Time.time - startTime > durationTime) {
			Recycle ();
		}	
		Move ();

	
	}
	public virtual void  Reset(){
	}
	public virtual void OnEnable(){
		transform.localScale = Vector3.one;
		startTime = Time.time;
//		SetLocalScale ();
	}
	void Move(){
		transform.position+= transform.right* moveVector * moveSpeed;
	}

	//设置移动方向子弹移动
	public void SetLocalScale(int mv){
		moveVector = mv;
		if (moveVector == 1) {
			//右
			if (transform.localScale.x < 0) {
				transform.localScale *= -1;
			}
		
		} else {
			//左
			if (transform.localScale.x > 0) {
				transform.localScale *= -1;
			} 
		}
		Debug.Log ("trap jade");
	}
	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag.Equals ("enemy")) {
			if (coll.gameObject.GetComponent<BaseEnemy> ().curHp <= 0) {
				return;
			}
			coll.gameObject.GetComponent<BaseEnemy> ().BeAttack (attackNum);
			//这里可以添加一些子弹击中特效动画 或粒子动画 
			Recycle();
		}
		if (coll.gameObject.tag.Equals ("rock")) {
			Destroy (coll.gameObject);
		}
	}
	void Recycle(){
		ParticleEffect.Instance.BulletOver (transform.position,diepoolName);
		SmartPool.Despawn (gameObject);
	}
}
