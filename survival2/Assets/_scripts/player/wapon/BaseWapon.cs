﻿using UnityEngine;
using System.Collections;

public class BaseWapon : GamePause {
	#region 公共属性
	[Header("基本属性")]
	public int wapon_id;
	public float attackNum;//攻击力
	public float attackRate;//攻击速度 单位秒 多少秒攻击一次

	[Header("用到的控制变量")]
	public bool is_attacking = false;//是否在进行攻击
	public bool is_using = false;//是否按了攻击键
	private float startAttackTime;

	public Transform hit_start;
	public Transform hit_end;


	private float durTime = 0;
	#endregion
	// Use this for initialization
	void Start () {
		startAttackTime = Time.time;
		is_attacking = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (paused) {
			return;
		}
		durTime = Time.time - startAttackTime;
	}

	#region 射线检测碰撞 检测攻击的对象
	public RaycastHit2D[] CheckHitGameObj(){
		int mask = 1 << LayerMask.NameToLayer ("enemy");
		RaycastHit2D[] hits =Physics2D.LinecastAll (hit_start.position, hit_end.position, mask);
		return hits;
	}

	public bool AttackEnemyAnim(){
		if (durTime > attackRate) {
			startAttackTime = Time.time;
			return true;
		} 
		return false;
	}

	public void AttackEneyHurt(){
		RaycastHit2D[] hits = CheckHitGameObj ();
		if (hits != null) {
			for (int i = 0; i < hits.Length; i++) {
				GameObject go = hits [i].collider.gameObject;
				if (go.tag.ToLower ().Equals ("enemy")) {
					go.GetComponent<BaseEnemy> ().BeAttack (attackNum);
				}
			}
		}
	}
	#endregion
}
