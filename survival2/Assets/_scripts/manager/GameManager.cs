﻿/// <summary>
/// Game manager. 游戏管理类 
/// 可以用些的随机生成敌人 TODO
/// </summary>
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameManager : Singleton<GameManager> {

	#region
	public int liveDays = 0;

	#endregion

	#region 预置怪物对象
	public string malePoolName;
	public float durationDelayTime = 3f;//出怪时间间隔
	#endregion

	#region 过程控制变量
	private float startTime ;
	#endregion
 	void  Awake(){
		base.Awake ();
//		
//		UserData userData = CommonUtil.LoadUserData ();
//		if (userData != null) {
//			Debug.Log ("userData === " + userData.name);
//		}
	}
	// Use this for initialization
	void Start () {
		startTime = Time.time ;
	}
	
	// Update is called once per frame
	void Update () {
		if (paused) {
			return;
		}
		if (Time.time - startTime > durationDelayTime) {
			startTime = Time.time;
			GeneratorEnemy ();
		}
	}

	void GeneratorEnemy(){
		var enemy = SmartPool.Spawn (malePoolName);
		enemy.transform.position = new Vector3 (-(Screen.width/2)/4,45);
	}

	internal void AddLiveDay(){
		liveDays++;
		UIController.Instance.AddTotalDayTxt ();
	}

}
